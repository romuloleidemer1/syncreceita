# Desafio tecnico Vaga Desenvolvedor de Sistemas PIX
Processamento de grandes arquivos utilizando Spring Boot ThreadPoolTaskExecutor para processamento em multithread.

## Problemática

### Cenário de Negócio:
Todo dia útil por volta das 6 horas da manhã um colaborador da retaguarda do Sicredi recebe e organiza as informações de contas para enviar ao Banco Central. Todas agencias e cooperativas enviam arquivos Excel à Retaguarda. Hoje o Sicredi já possiu mais de 4 milhões de contas ativas.
Esse usuário da retaguarda exporta manualmente os dados em um arquivo CSV para ser enviada para a Receita Federal, antes as 10:00 da manhã na abertura das agências.

### Requisito:
Usar o "serviço da receita" (fake) para processamento automático do arquivo.

### Funcionalidade:
1. Criar uma aplicação SprintBoot standalone. Exemplo: java -jar SincronizacaoReceita <input-file>
2. Processa um arquivo CSV de entrada com o formato abaixo.
3. Envia a atualização para a Receita através do serviço (SIMULADO pela classe ReceitaService).
4. Retorna um arquivo com o resultado do envio da atualização da Receita. Mesmo formato adicionando o resultado em uma nova coluna.


### Formato CSV:
```
agencia;conta;saldo;status
0101;12225-6;100,00;A
0101;12226-8;3200,50;A
3202;40011-1;-35,12;I
3202;54001-2;0,00;P
3202;00321-2;34500,00;B
...
```

## Ferramentas utilizadas
- spring-boot-starter **2.4.0**

## Modo de execução
java -jar batch-processing-sicredi-1.0-SNAPSHOT.jar --accountFilePath="\<FilePath\>"
