package br.com.leidemer.syncReceita.thread;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.leidemer.syncReceita.entity.Conta;
import br.com.leidemer.syncReceita.services.ReceitaService;

@Component
@Scope("prototype")
//Tarefa que sera executada na Thread
public class Task implements Runnable {
	
	private static String SUCESSO = "SUCESSO";
	private static String ERRO_PROCESSO = "ERRO NO PROCESSO DA RECEITA";
	private static String ERRO_INESPERADO = "ERRO NA FORMATACAO DO SALDO";
	
	private Conta conta;
	private BufferedWriter out;

	public Task(Conta conta, BufferedWriter out) {
		this.conta = conta;
		this.out = out;
	}

	public Conta getConta() {
		return conta;
	}

	public BufferedWriter getOut() {
		return out;
	}
	
	//Executa o processo com a receita
	//Escreve o resultado no arquivo atraves do BufferedWriter
	public synchronized void run() {
		ReceitaService receitaService = new ReceitaService();
		String resultado = SUCESSO;	
		String saldo = null;
			
		DecimalFormat df = new DecimalFormat("0.00");
		saldo = df.format(conta.getSaldo());
	
		
		try {
			if (!receitaService.atualizarConta(conta.getAgencia(), conta.getConta(), conta.getSaldo(), conta.getStatus())) {
				resultado = ERRO_PROCESSO;
			}
		} catch (RuntimeException e) {
			resultado = ERRO_INESPERADO;
			e.printStackTrace();
		} catch (InterruptedException e) {
			resultado = ERRO_INESPERADO;
			e.printStackTrace();
		}
		
		try {
			getOut().write(conta.getAgencia()+";"+ conta.getConta() +";"+ saldo +";"+ conta.getStatus() + ";" + resultado);
			getOut().newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}
