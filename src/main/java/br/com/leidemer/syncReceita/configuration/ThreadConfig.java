package br.com.leidemer.syncReceita.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadConfig {
	
	public static int MAX_THREADS = 100;

	@Bean
	//Configuro o bean ThreadPoolTaskExecutor
	public ThreadPoolTaskExecutor getExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setMaxPoolSize(MAX_THREADS);
		threadPoolTaskExecutor.setThreadNamePrefix("async-exec");
		threadPoolTaskExecutor.setCorePoolSize(10);
		threadPoolTaskExecutor.setQueueCapacity(0);
		threadPoolTaskExecutor.initialize();
		return threadPoolTaskExecutor;
	}
	
}
