package br.com.leidemer.syncReceita.configuration;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import br.com.leidemer.syncReceita.entity.Conta;

@Component
public class ContaProcessor {
	
	private static String COMMA_DELIMITER = ";";

	//Realiza um parse simples
	//Utilizando split para realizar a quebra das colunas
	//Transforma o Texto para Double e remove o "-" da conta
	public Conta parseLineToConta(String line) {					
		String[] values = line.split(COMMA_DELIMITER);        
        
        Conta contas = new Conta();
        contas.setAgencia(values[0]);
        contas.setConta(values[1].replace("-", ""));
        contas.setSaldo(0d);
        
        NumberFormat nf = NumberFormat.getInstance(new Locale ("pt", "BR"));
        try {
        	contas.setSaldo(nf.parse(values[2]).doubleValue());
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        contas.setStatus(values[3]);
		
		return contas;
	}
	
}

