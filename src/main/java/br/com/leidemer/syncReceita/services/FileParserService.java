package br.com.leidemer.syncReceita.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.leidemer.syncReceita.configuration.ContaProcessor;
import br.com.leidemer.syncReceita.entity.Conta;
import br.com.leidemer.syncReceita.thread.Task;

@Service
public class FileParserService {

	public static enum Status {
		PARADO, EXECUTANDO, CONCLUIDO
	}

	private static String ARQUIVO_DESTINO = "/home/rleidemer/data/sicredi/outputDataNew.csv";

	@Autowired
	TaskProcessorService taskProcessor;

	@Autowired
	ContaProcessor contaProcessor;

	private Status status;

	private BufferedReader bufferedReader;

	private static BufferedWriter bufferedWriterSync;

	// Utilizei syncronized para garantir que apenas uma instacia
	// da thread pode escrever por vez
	private static synchronized BufferedWriter getWriter() {
		try {
			if (bufferedWriterSync == null) {
				bufferedWriterSync = new BufferedWriter(new FileWriter(ARQUIVO_DESTINO, false));
			}

			return bufferedWriterSync;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static synchronized void closeWriter() {
		if (bufferedWriterSync != null) {
			try {
				bufferedWriterSync.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Carrego o arquivo
	public void loadFile(File file) throws IOException {
		bufferedReader = new BufferedReader(new FileReader(file));
	}

	// Metodo processar percorre o arquivo linha por linha
	// Sem colocar os dados em memoria
	// Pulando a primeira linha
	// Escreve o cabecalho do arquivo
	// Realiza um parse da linha para o objeto conta
	// Executa a tarefa em uma thread asyncrona
	// Ao termino fecha os buffers utilizados
	public void processar() throws InterruptedException, IOException {
		status = Status.EXECUTANDO;

		String readLine = "";
		int lines = 0;

		escreverCabecalho();

		while ((readLine = bufferedReader.readLine()) != null) {
			lines++;
			if (lines == 1) {
				continue;
			}

			Conta contas = contaProcessor.parseLineToConta(readLine);

			Task task = new Task(contas, getWriter());
			taskProcessor.execute(task);
		}

		taskProcessor.finalizar();

		status = Status.CONCLUIDO;
		closeWriter();
		bufferedReader.close();
	}

	// Escreve o cabecalho do arquivo
	private void escreverCabecalho() throws IOException {
		getWriter().write("agencia;conta;saldo;status");
		getWriter().newLine();
	}

	public Status getStatus() {
		return status;
	}

}
