package br.com.leidemer.syncReceita.services;

import java.util.concurrent.RejectedExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import br.com.leidemer.syncReceita.configuration.ThreadConfig;
import br.com.leidemer.syncReceita.thread.Task;

@Service
public class TaskProcessorService {
	
	@Autowired
	ThreadPoolTaskExecutor threadPoolTaskExecutor; 
	
	//Metodo que utiliza o pool ThreadPoolTaskExecutor
	//Para executar o processamento de forma asyncrona
	//Caso tenha atingido o maximo de threads, aguarda liberacao
	public void execute(Task task) throws InterruptedException {
		boolean sucesso = false;
		
		while (!sucesso) {
			while (maxActiveThreads()) {
				Thread.sleep(100);			
			}
			
			try  {
				threadPoolTaskExecutor.execute(task);
				sucesso = true;
			} catch (RejectedExecutionException e) {
				//Caso ocorra erro, ira tentar enfileirar o processamento até um slot livre				
			}
		}		
	}
	
	private boolean maxActiveThreads() {		
		return (threadPoolTaskExecutor.getActiveCount() == ThreadConfig.MAX_THREADS);
	}
	
	//Aguarda finalizar todo o processamento
	public void finalizar() throws InterruptedException {
		while (threadPoolTaskExecutor.getActiveCount() > 0) {
			Thread.sleep(100);
		}
	}
	

}
