package br.com.leidemer.syncReceita.entity;

public class Conta {

	private long id;

    private String agencia;
    
    private String conta;

    private Double saldo;
    
    private String status;
    
    private String resultado;

    public Conta() {
    }

    public Conta(final String agencia, final String conta, final Double saldo, final String status) {
        this.agencia = agencia;
        this.conta = conta;
        this.saldo = saldo;
        this.status = status;       
    }

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResult(String resultado) {
		this.resultado = resultado;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "["+ agencia + "-" + conta + "] - R$ " + saldo + "("+status+") -> "+ resultado;
	}
}
